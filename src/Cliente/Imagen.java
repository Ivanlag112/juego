/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import java.awt.Image;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 *
 * @author Ivan
 */
public class Imagen extends Thread {

    boolean activo = true;
    Random img = new Random();
    Random tiem = new Random();
    int i = 0;
    int tiempo = 0;
    int c = 0;
    int SEL;

    public Imagen(int dato) {
        this.SEL = dato;
    }

    public void run() {
        while (activo == true) {
            i = img.nextInt(5);
            tiempo = tiem.nextInt(250);
            if (i == 1) {
                if (SEL == 1) {
                    InterfazJ.Imagen1.setIcon(new ImageIcon(InterfazJ.Bananas.getImage().getScaledInstance(InterfazJ.Imagen1.getWidth(), InterfazJ.Imagen1.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 2) {
                    InterfazJ.Imagen2.setIcon(new ImageIcon(InterfazJ.Bananas.getImage().getScaledInstance(InterfazJ.Imagen2.getWidth(), InterfazJ.Imagen2.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 3) {
                    InterfazJ.Imagen3.setIcon(new ImageIcon(InterfazJ.Bananas.getImage().getScaledInstance(InterfazJ.Imagen3.getWidth(), InterfazJ.Imagen3.getHeight(), Image.SCALE_DEFAULT)));
                }
                c = 1;
            }
            if (i == 2) {
                if (SEL == 1) {
                    InterfazJ.Imagen1.setIcon(new ImageIcon(InterfazJ.Diamante.getImage().getScaledInstance(InterfazJ.Imagen1.getWidth(), InterfazJ.Imagen1.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 2) {
                    InterfazJ.Imagen2.setIcon(new ImageIcon(InterfazJ.Diamante.getImage().getScaledInstance(InterfazJ.Imagen2.getWidth(), InterfazJ.Imagen2.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 3) {
                    InterfazJ.Imagen3.setIcon(new ImageIcon(InterfazJ.Diamante.getImage().getScaledInstance(InterfazJ.Imagen3.getWidth(), InterfazJ.Imagen3.getHeight(), Image.SCALE_DEFAULT)));
                }
                c = 2;
            }
            if (i == 3) {
                if (SEL == 1) {
                    InterfazJ.Imagen1.setIcon(new ImageIcon(InterfazJ.Uvas.getImage().getScaledInstance(InterfazJ.Imagen1.getWidth(), InterfazJ.Imagen1.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 2) {
                    InterfazJ.Imagen2.setIcon(new ImageIcon(InterfazJ.Uvas.getImage().getScaledInstance(InterfazJ.Imagen2.getWidth(), InterfazJ.Imagen2.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 3) {
                    InterfazJ.Imagen3.setIcon(new ImageIcon(InterfazJ.Uvas.getImage().getScaledInstance(InterfazJ.Imagen3.getWidth(), InterfazJ.Imagen3.getHeight(), Image.SCALE_DEFAULT)));
                }
                c = 3;
            }
            if (i == 4) {
                if (SEL == 1) {
                    InterfazJ.Imagen1.setIcon(new ImageIcon(InterfazJ.Sandia.getImage().getScaledInstance(InterfazJ.Imagen1.getWidth(), InterfazJ.Imagen1.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 2) {
                    InterfazJ.Imagen2.setIcon(new ImageIcon(InterfazJ.Sandia.getImage().getScaledInstance(InterfazJ.Imagen2.getWidth(), InterfazJ.Imagen2.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 3) {
                    InterfazJ.Imagen3.setIcon(new ImageIcon(InterfazJ.Sandia.getImage().getScaledInstance(InterfazJ.Imagen3.getWidth(), InterfazJ.Imagen3.getHeight(), Image.SCALE_DEFAULT)));
                }
                c = 4;
            }
            if (i == 5) {
                if (SEL == 1) {
                    InterfazJ.Imagen1.setIcon(new ImageIcon(InterfazJ.Siete.getImage().getScaledInstance(InterfazJ.Imagen1.getWidth(), InterfazJ.Imagen1.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 2) {
                    InterfazJ.Imagen2.setIcon(new ImageIcon(InterfazJ.Siete.getImage().getScaledInstance(InterfazJ.Imagen2.getWidth(), InterfazJ.Imagen2.getHeight(), Image.SCALE_DEFAULT)));
                } else if (SEL == 3) {
                    InterfazJ.Imagen3.setIcon(new ImageIcon(InterfazJ.Siete.getImage().getScaledInstance(InterfazJ.Imagen3.getWidth(), InterfazJ.Imagen3.getHeight(), Image.SCALE_DEFAULT)));
                }
                c = 5;
            }
            try {
                sleep(tiempo);
            } catch (InterruptedException e) {
            }
        }
    }
}
