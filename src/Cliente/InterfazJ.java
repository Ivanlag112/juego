/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Ivan
 */
public class InterfazJ extends javax.swing.JFrame {

    Imagen img1 = new Imagen(1);
    Imagen img2 = new Imagen(2);
    Imagen img3 = new Imagen(3);
    int z = 0;
    static ImageIcon Diamante = new ImageIcon("Diamante.jpg");
    static ImageIcon Bananas = new ImageIcon("Bananas.jpg");
    static ImageIcon Sandia = new ImageIcon("Sandia.jpg");
    static ImageIcon Siete = new ImageIcon("Siete.png");
    static ImageIcon Uvas = new ImageIcon("uvas.jpg");
    static ImageIcon ENca = new ImageIcon("Encabezado.jpg");
    public int mon;
    Socket sc;
    PrintWriter out;
    BufferedReader in;
    InetAddress address;

    public InterfazJ(String usr) {
        initComponents();
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(600, 450));
        setMaximumSize(new Dimension(800, 600));
        Imagen1.setIcon(new ImageIcon(Siete.getImage().getScaledInstance(Imagen1.getWidth(), Imagen1.getHeight(), Image.SCALE_DEFAULT)));
        Imagen2.setIcon(new ImageIcon(Siete.getImage().getScaledInstance(Imagen2.getWidth(), Imagen2.getHeight(), Image.SCALE_DEFAULT)));
        Imagen3.setIcon(new ImageIcon(Siete.getImage().getScaledInstance(Imagen3.getWidth(), Imagen3.getHeight(), Image.SCALE_DEFAULT)));
        ImagenEN.setIcon(new ImageIcon(ENca.getImage().getScaledInstance(ImagenEN.getWidth(), ImagenEN.getHeight(), Image.SCALE_DEFAULT)));
        repaint();
        Usuario.setText(usr);
        Tudinero.setText(String.valueOf(mon));
        try {
            this.address = InetAddress.getLocalHost();
            sc = new Socket(address.getHostAddress(), 9999);
            in = new BufferedReader(new InputStreamReader(sc.getInputStream()));
            out = new PrintWriter(sc.getOutputStream(), true);
            out.println(Usuario.getText());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion");
            System.exit(0);
            Logger.getLogger(InterfazJ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cerrar() {
        Object[] opciones = {"Aceptar", "Cancelar"};
        int eleccion = JOptionPane.showOptionDialog(rootPane, "En realidad desea realizar cerrar la aplicacion", "Mensaje de Confirmacion",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, opciones, "Aceptar");
        if (eleccion == JOptionPane.YES_OPTION) {
            try {
                out.println("Salir");
                sc.close();
                System.exit(0);
            } catch (IOException ex) {
                Logger.getLogger(InterfazJ.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Imagen1 = new javax.swing.JLabel();
        Imagen2 = new javax.swing.JLabel();
        Imagen3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        DC = new javax.swing.JTextField();
        APUS = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        ImagenEN = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        Usuario = new javax.swing.JTextField();
        Tirar = new javax.swing.JButton();
        Detener = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        Tudinero = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Jackpot");
        setBackground(new java.awt.Color(204, 0, 0));
        setPreferredSize(new java.awt.Dimension(650, 500));
        setResizable(false);
        setSize(new java.awt.Dimension(650, 500));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        DC.setEditable(false);
        DC.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        DC.setText("0");

        APUS.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        APUS.setText("0");

        jLabel4.setText("Botin disponible");

        jLabel5.setText("Apuesta ");

        ImagenEN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ImagenEN.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ImagenEN.setMaximumSize(new java.awt.Dimension(200, 81));
        ImagenEN.setMinimumSize(new java.awt.Dimension(200, 81));
        ImagenEN.setName(""); // NOI18N

        jLabel1.setText("Usuario:");

        Usuario.setEditable(false);

        Tirar.setText("Tirar!");
        Tirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TirarActionPerformed(evt);
            }
        });

        Detener.setText("Detener!");
        Detener.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DetenerActionPerformed(evt);
            }
        });

        jLabel2.setText("Dinero:");

        Tudinero.setEditable(false);

        jButton1.setText("Refrescar  botin");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Imagen1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Imagen2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Imagen3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jSeparator1)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(206, 206, 206)
                .addComponent(ImagenEN, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(8, 8, 8)
                        .addComponent(APUS, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Tudinero, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addGap(42, 42, 42)
                        .addComponent(DC, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Tirar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Detener)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ImagenEN, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(Imagen1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Imagen2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Imagen3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(DC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(Tudinero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(APUS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Tirar)
                            .addComponent(Detener)
                            .addComponent(jButton1))))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TirarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TirarActionPerformed

        if (img1.isAlive() == false) {
            if (img2.isAlive() == false) {
                if (img3.isAlive() == false) {
                    img1 = new Imagen(1);
                    img2 = new Imagen(2);
                    img3 = new Imagen(3);
                    img1.activo = true;
                    img2.activo = true;
                    img3.activo = true;
                    img1.start();
                    img2.start();
                    img3.start();
                    z = 1;
                }
            }
        }
    }//GEN-LAST:event_TirarActionPerformed

    private void DetenerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DetenerActionPerformed

        try {
            if (z == 1) {
                String Dinero = this.Tudinero.getText();
                String Apue = this.APUS.getText();
                String Nom = this.Usuario.getText();
                int dine = Integer.parseInt(Dinero);
                int apue = Integer.parseInt(Apue);
                img1.activo = false;
                img2.activo = false;
                img3.activo = false;
                if ((img1.c == img2.c) && (img1.c == img3.c)) {
                    if (dine > apue && apue > 0) {
                        out.println("1");
                        out.println(Apue);
                        out.println(Dinero);
                        out.println(Nom);
                        String cm = in.readLine();
                        String dn = in.readLine();
                        DC.setText(cm);
                        this.Tudinero.setText(dn);
                        JOptionPane.showMessageDialog(null, "Ganaste!");
                        z = 0;
                    } else {
                        JOptionPane.showMessageDialog(null, "Apuesta no valida");
                        z = 0;
                    }

                } else {
                    if (dine > apue && apue > 0) {
                        out.println("2");
                        out.println(Apue);
                        out.println(Dinero);
                        out.println(Nom);
                        String cm = in.readLine();
                        String dn = in.readLine();
                        DC.setText(cm);
                        this.Tudinero.setText(dn);
                        JOptionPane.showMessageDialog(null, "Perdiste!");
                        z = 0;
                    } else {
                        JOptionPane.showMessageDialog(null, "Apuesta no valida");
                        z = 0;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "No has tirado");
            }
        } catch (HeadlessException | IOException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion");
            Logger.getLogger(InterfazJ.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }//GEN-LAST:event_DetenerActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        cerrar();
    }//GEN-LAST:event_formWindowClosing

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
                out.println("3");
                try {
                    DC.setText(in.readLine());
                } catch (IOException ex) {
                    Logger.getLogger(InterfazJ.class.getName()).log(Level.SEVERE, null, ex);
                }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField APUS;
    public static javax.swing.JTextField DC;
    private javax.swing.JButton Detener;
    public static javax.swing.JLabel Imagen1;
    public static javax.swing.JLabel Imagen2;
    public static javax.swing.JLabel Imagen3;
    private javax.swing.JLabel ImagenEN;
    private javax.swing.JButton Tirar;
    public javax.swing.JTextField Tudinero;
    public javax.swing.JTextField Usuario;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables
}
