/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import Cliente.Login;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ivan
 *
 */
public class Server2 {

    public static Panel Pan = new Panel();

    public static void main(String[] args) throws IOException {

        int portNumber = 9999;
        Pan.setVisible(true);
        try (
                ServerSocket serverSocket
                = new ServerSocket(portNumber);) {
            Pan.PanelT.append("--Servidor iniciado--");
            while (true) {
                (new Proceso(serverSocket.accept())).start();
            }

        } catch (IOException e) {
            System.out.println(" Error al tratar de conectar");
            System.out.println(e.getMessage());
        } catch (SQLException ex) {
            //Logger.getLogger(Server2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

class Proceso extends Thread {

    Socket clientSocket;
    ResultSet res;
    PreparedStatement stat = null;
    Connection c = null;
    ResultSetMetaData rs;

    public Proceso(Socket socket) throws SQLException {
        this.clientSocket = socket;
        c = DriverManager.getConnection("jdbc:mysql://localhost:3306/juego?zeroDateTimeBehavior=convertToNull", "Root", "24681012a");
    }
//UPDATE refranero SET fecha="2003-06-01" WHERE ID=1;

    public int obtenerdinero() {
        int dien = 0;
        try {
            stat = c.prepareStatement("SELECT * FROM casa WHERE Total;");
            res = stat.executeQuery();
            rs = res.getMetaData();
            res.next();
            dien = res.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return dien;
        }

    }

    public void modificardinerobasededatos(int dinero) {
        try {
            stat = c.prepareStatement("UPDATE casa SET Total=? WHERE Total;");
            stat.setInt(1, dinero);
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modificardinerobasededatos1(int dinero, String usr) {
        try {
            stat = c.prepareStatement("UPDATE usuarios SET Dinero=? WHERE Nombre=?;");
            stat.setInt(1, dinero);
            stat.setString(2, usr);
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        try (
                PrintWriter out
                = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));) {
            String inputLine, ss = "", ss1 = "", ss3 = "";
            String usuario = in.readLine();
            if (usuario != null) {
                Server2.Pan.PanelT.append("\nEl usuaio " + usuario + " se conecto desde: " + clientSocket.getRemoteSocketAddress().toString());
            }
            while ((inputLine = in.readLine()) != null) {
                if (!inputLine.equals("3") && !inputLine.equals("Salir")) {
                    int dbd = obtenerdinero();
                    int apue = Integer.parseInt(in.readLine());
                    int dine = Integer.parseInt(in.readLine());
                    String name = in.readLine();
                    if (inputLine.equals("1")) {
                        modificardinerobasededatos((dbd - apue));
                        modificardinerobasededatos1((dine + apue), name);
                        ss = String.valueOf(dbd - apue);
                        ss1 = String.valueOf(dine + apue);
                        ss3 = "\n" + usuario + " gano: " + apue;
                        Server2.Pan.PanelT.append(ss3);
                        Server2.Pan.PanelT.append("\nBotin disponible :" + obtenerdinero());
                        out.println(ss);
                        out.println(ss1);
                        out.flush();
                    } else if (inputLine.equals("2")) {
                        modificardinerobasededatos((dbd + apue));
                        modificardinerobasededatos1((dine - apue), name);
                        ss = String.valueOf(dbd + apue);
                        ss1 = String.valueOf(dine - apue);
                        ss3 = "\n" + usuario + " perdio: " + apue;
                        out.println(ss);
                        out.println(ss1);
                        Server2.Pan.PanelT.append(ss3);
                        Server2.Pan.PanelT.append("\nBotin disponible :" + obtenerdinero());
                        out.flush();
                    }
                } else if (inputLine.equals("Salir")) {
                    ss3 = "\n" + usuario + " se desconecto";
                    Server2.Pan.PanelT.append(ss3);

                } else if (inputLine.equals("3")) {
                    String s = String.valueOf(obtenerdinero());
                    out.println(s);
                }
            }

        } catch (IOException e) {
            System.out.println("Error I/O");
            System.out.println();
        } catch (Exception e1) {
            System.out.println(e1);
        }
    }
}
